import { Component, OnInit } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { state, trigger, transition, style, animate } from '@angular/animations';
// other import definitions

@Component({
  selector: 'app-animationdemo',
  templateUrl: './animationdemo.component.html',
  styleUrls: ['./animationdemo.component.css'],
  animations: [
    trigger('changeDivSize', [
      state('initial', style({
        backgroundColor: '#ffffff',
        width: '0px',
        height: '0px',
        opacity: 0
      })),
      state('final', style({
        backgroundColor: '#ffffff',
        width: '200px',
        height: '60px'
      })),
      transition('initial=>final', animate('1000ms')),
      transition('final=>initial', animate('1000ms'))
    ]),
    trigger('changeDivSize_1s', [
      state('initial', style({
        backgroundColor: '#ffffff',
        width: '0px',
        height: '0px',
        opacity: 0
      })),
      state('final', style({
        backgroundColor: '#ffffff',
        width: '300px',
        height: '550px',

      })),
      transition('initial=>final', animate('1000ms')),
      transition('final=>initial', animate('1000ms'))
    ]),
    trigger('balloonEffect', [
      state('initial', style({
        backgroundColor: '#8DFCF1',
        transform: 'scale(1)'
      })),
      state('final', style({
        backgroundColor: '#8DFCF1',
        transform: 'scale(1)'
      })),
      transition('final=>initial', animate('1000ms')),
      transition('initial=>final', animate('1000ms'))
    ])
  ]
})
export class AnimationdemoComponent implements OnInit {
  currentState = 'initial';

  constructor() { }

  ngOnInit() {
  }
  changeState() {

    this.currentState = this.currentState === 'initial' ? 'final' : 'initial';
  }
  formatSubtitle = (percent: number) : string => {
    
    if(percent >= 100){
      return "Congratulations!"
    }else if(percent >= 50){
      return "Half"
    }else if(percent > 0){
      return "Just began"
    }else {
      return "Not started"
    }
  }
}
